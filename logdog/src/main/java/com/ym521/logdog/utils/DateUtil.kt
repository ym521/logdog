package com.ym521.logdog.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/7/27
 *explain:
 *
 */
internal object DateUtil {
    private val dateFormat by lazy {
        SimpleDateFormat("yyyy-MM-dd")
    }

    private val dateFormatMessage by lazy {
        SimpleDateFormat("yyyy:MM:dd HH:mm:ss.SSS")
    }

    fun currentDateFile(): String {
        return dateFormat.format(Calendar.getInstance().time)
    }

    fun currentDateMessage(): String {
        return dateFormatMessage.format(Calendar.getInstance().time)
    }


}