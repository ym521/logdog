package com.ym521.logdog.utils

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/7/26
 *explain:
 *
 */
internal object StringCodeUtil {

    fun encryptionString(content: String): ByteArray {
        val byteArray = content.toByteArray(Charsets.UTF_8)
        for (index in byteArray.indices) {
            byteArray[index] = encryptionByte(byteArray[index])
        }
        return byteArray
    }

    private fun encryptionByte(code: Byte): Byte {
        val ic = code.toInt() and 0xFF
        val l4 = ic shr 4
        val r4: Int = ic and 0x0F shl 4
        val c8 = l4 or r4
        return c8.toByte()
    }
}