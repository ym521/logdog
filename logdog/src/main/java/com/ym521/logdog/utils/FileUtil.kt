package com.ym521.logdog.utils

import java.io.File

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/7/27
 *explain:
 *
 */
internal object FileUtil {

    fun createDirs(file: File) {
        if (!file.exists()) {
            file.mkdirs()
        }
    }

    fun createFile(file: File) {
        if (!file.parentFile?.exists()!!) {
            file.parentFile?.mkdirs()
        }
        if (!file.exists()) {
            file.createNewFile()
        }
    }

}