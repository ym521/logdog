package com.ym521.logdog.utils

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/6/1
 *explain:
 * 缓存当前日期
 */
internal object CacheUtil {
    private const val LOGDOG_CACHE = "logdog_cache"
    private var sp: SharedPreferences? = null

    fun initCache(mContext: Context) {
        sp = mContext.getSharedPreferences(LOGDOG_CACHE, Context.MODE_PRIVATE)
    }


    fun saveCurrentLogInfo(date: String, path: String) {
        sp?.edit()?.
        putString("CurrentDate", date)?.
        putString("CurrentPath", path)?.
        apply()
    }

    fun getCurrentLogInfo(): Array<String> {
        val data = Array(2) { "" }
        data[0] = sp?.getString("CurrentDate", "") ?: ""
        data[1] = sp?.getString("CurrentPath", "") ?: ""
        return data
    }

}