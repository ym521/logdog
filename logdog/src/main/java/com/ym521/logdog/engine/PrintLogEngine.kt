package com.ym521.logdog.engine

import android.util.Log
import com.ym521.logdog.core.ILogDogFormatEngine
import com.ym521.logdog.core.IPrintLogEngine
import com.ym521.logdog.core.LogDogConfig
import com.ym521.logdog.core.LogPriority


/**
 * @author YM
 * @E-mail 2435970206@qq
 * @QQ 2435970206
 * 日志打印引擎
 */
internal class PrintLogEngine private constructor() : IPrintLogEngine {
    private val formatEngine: ILogDogFormatEngine = LogDogFormatEngine.build()

    companion object {
        fun build(): IPrintLogEngine {
            return PrintLogEngine()
        }
    }

    override fun logPrint(priority: LogPriority, tag: String, msg: String) {
        val logPriority = when (priority) {
            LogPriority.DEBUG -> Log.DEBUG
            LogPriority.INFO -> Log.INFO
            LogPriority.WARN -> Log.WARN
            LogPriority.ERROR -> Log.ERROR
            LogPriority.CRASH -> Log.ERROR
        }
        if (LogDogConfig.isNewLogCat) {
            formatEngine.formatGenerateNewLogcat(tag, msg) {
                Log.println(logPriority, tag, it)
            }
        } else {
            formatEngine.formatGenerate(msg) {
                Log.println(logPriority, tag, it)
            }
        }
    }

}