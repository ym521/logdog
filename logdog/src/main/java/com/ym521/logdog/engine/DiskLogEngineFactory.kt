package com.ym521.logdog.engine

import android.content.Context
import android.os.HandlerThread
import com.ym521.logdog.core.IDiskLogEngine
import com.ym521.logdog.core.LogDogConfig
import com.ym521.logdog.utils.FileUtil

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/7/26
 *explain:
 *
 */
internal object DiskLogEngineFactory {
    private lateinit var handlerThread: HandlerThread

    //日志文件 根目录
    var ROOT_DIR = ""

    fun build(crashLog: Boolean, mContext: Context): IDiskLogEngine {
        if (ROOT_DIR.isNotBlank()) {
            val fileDir = if (LogDogConfig.logFileShow) {
                mContext.getExternalFilesDir("logdog")!!.absoluteFile
            } else {
                mContext.getDir("logdog", Context.MODE_PRIVATE).absoluteFile
            }
            FileUtil.createFile(fileDir)
            ROOT_DIR = fileDir.absolutePath
        }
        if (!this::handlerThread.isInitialized) {
            handlerThread = HandlerThread("DiskFileThread")
            handlerThread.start()
        }
        return if (crashLog) {
            DiskCrashLogEngine.create(handlerThread.looper)
        } else {
            DiskLogEngine.create(handlerThread.looper)
        }
    }


}