package com.ym521.logdog.core

/**
 * @author YM
 * @E-mail 2435970206@qq
 * @QQ 2435970206
 * log格式引擎 API
 */
internal interface ILogDogFormatEngine {

    fun formatGenerate(body: String,printlnLog:(String)->Unit)

    fun formatGenerateNewLogcat(tag: String, body: String,printlnLog:(String)->Unit)

}