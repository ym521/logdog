package com.ym521.logdog.core

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/6/1
 *explain:
 * 日志文件类型
 */
enum class LogFileType {
    LOG_FILE, //普通日志文件  默认
    LOG_CRASH_FILE; ////普通Crash日志文件
}