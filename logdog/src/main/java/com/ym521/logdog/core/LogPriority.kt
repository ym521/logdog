package com.ym521.logdog.core

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/6/1
 *explain: log 优先级枚举
 */
enum class LogPriority(val priority: Int) {
    DEBUG(0x02), //对应 log debug
    INFO(0x04),  //对应 log info
    WARN(0x08), //对应 log warn
    ERROR(0x10), //对应 log error

    CRASH(0x20); // Crash 崩溃 注意：这个类型将日志写入崩溃日志文件中
}