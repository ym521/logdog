package com.ym521.logdog.core

import android.content.Context

/**
 * @author YM
 * @E-mail 2435970206@qq
 * @QQ 2435970206
 * 对外提供服务 API
 */
interface ILogDogEngine {

    fun <T> debug(tag: String, msg: T)

    fun debug(tag: String, msgFormat: String, vararg objs: Any)

    fun <T> info(tag: String, msg: T)

    fun info(tag: String, msgFormat: String, vararg objs: Any)

    fun <T> warn(tag: String, msg: T)

    fun warn(tag: String, msgFormat: String, vararg objs: Any)

    fun <T> error(tag: String, msg: T)

    fun error(tag: String, msgFormat: String, vararg objs: Any)

    fun <T> custom(priority: LogPriority, tag: String, msg: T, printer: Boolean, write: Boolean)

    fun shareLogFile(mContext: Context,fileType: LogFileType)

    fun crashLog(tag: String,msgFormat: String,vararg objs: Any)

    fun custom(
        printer: Boolean,
        write: Boolean,
        priority: LogPriority,
        tag: String,
        msgFormat: String,
        vararg objs: Any
    )

}