package com.ym521.logdog.core

/**
 * @author YM
 * @E-mail 2435970206@qq
 * @QQ 2435970206
 * 日志输出引擎API
 */
internal interface IPrintLogEngine {

    fun logPrint(priority:LogPriority,tag:String,msg:String)

}