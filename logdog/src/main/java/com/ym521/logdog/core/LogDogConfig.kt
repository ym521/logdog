package com.ym521.logdog.core

import com.ym521.logdog.engine.LogDogEngine

/**
 * @author YM
 * @E-mail 2435970206@qq
 * @QQ 2435970206
 * LogDog 日志看门狗的配置
 * 基本logdog的可配置信息都在这里进行配置开关
 * 或者其他标注信息等
 */
internal object LogDogConfig {
    //是否显示线程信息
    var isShowThreadInfo: Boolean = false

    //是否显示在执行的方法
    var isShowMethod: Boolean = false

    //显示在执行的方法的个数
    var showMethodCount: Int = 2

    //是否写日志文件
    var isWriteLogFileEnable: Boolean = false

    //是否显示在外部私有空间
    var logFileShow: Boolean = false

    //是否需要对日志文件进行AES 加密
    var isLogFileAES: Boolean = false

    //对日志文件进行AES 加密的秘钥
    var logFileAES_key: String = "0000000000000000"

    //未设置的TAG 的缺省TAG
    var TAG = "LOGDOG"

    //打印日志过滤 日志级别 默认全开 打印
    var logFilter: Int = 0

    //日志写入log文件 过滤配置 过滤 日志级别 默认全开 打印
    var logWriteFilter: Int = 0

    //自定义 方法栈 打印深度
    var extraOffset: Int = 0

    //自动换行
    var isWrap: Boolean = false

    //是否日志文件编码混淆 默认不混淆
    var encoding: Boolean = false

    //日志文件的分割大小 MB
    var logFileSize = 10

    //是否是新版本LogCat
    var isNewLogCat = false
}