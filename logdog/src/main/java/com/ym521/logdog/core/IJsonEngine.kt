package com.ym521.logdog.core

/**
 * @author YM
 * @E-mail 2435970206@qq
 * @QQ 2435970206
 * 由用户自行实现 Json引擎 API
 * 建议使用 Google 的Gson
 */
interface IJsonEngine {

    fun toJSON(obj: Any): String

}