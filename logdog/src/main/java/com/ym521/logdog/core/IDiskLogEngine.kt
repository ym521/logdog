package com.ym521.logdog.core


/**
 * @author YM
 * @E-mail 2435970206@qq
 * @QQ 2435970206
 * 日志狗的 log磁盘写入引擎 API
 */
internal interface IDiskLogEngine {


   fun logWrite(priority:LogPriority,tag:String,msg:String)

}