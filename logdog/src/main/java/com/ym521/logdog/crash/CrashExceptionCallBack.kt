package com.ym521.logdog.crash

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2023/10/29
 *explain: APP 异常回调 开发人员可以自行处理
 *如果没有实现 则执行默认处理
 * 注意 回调是在子线程内
 */
interface CrashExceptionCallBack {

    fun onMainThreadException(t: Throwable)

    fun onChildThreadException(t: Throwable)
}