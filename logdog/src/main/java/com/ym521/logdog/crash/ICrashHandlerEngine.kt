package com.ym521.logdog.crash

import com.ym521.logdog.core.ILogDogEngine

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2023/10/29
 *explain: APP 崩溃处理引擎
 *
 */
internal interface ICrashHandlerEngine {

    fun onCrashHandler(logDog: ILogDogEngine)

    fun setCustomHandler(callback: CrashExceptionCallBack? = null)

}