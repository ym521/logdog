package com.ym521.logdog.provider

import androidx.core.content.FileProvider

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2023/9/3
 *explain: 日志文件分享给其他应用
 *
 */
class LogFileShareProvider : FileProvider() {

}