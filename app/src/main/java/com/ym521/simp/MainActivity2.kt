package com.ym521.simp

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.ym521.logdog.LogDog

class MainActivity2 : AppCompatActivity() {
    private val TAG = "MainActivity2"

    lateinit var view:View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        LogDog.debug(TAG, "onCreate")
//      findViewById<View>(R.id.tvDedug)
//            .postDelayed({
//            startActivity(Intent(this, MainActivity3::class.java))
//        }, 10 * 1000)

        view.post {
            LogDog.debug(TAG, "onCreate")
        }
    }


    override fun onStart() {
        super.onStart()
        LogDog.debug(TAG, "onStart")
    }

    override fun onResume() {
        super.onResume()
        LogDog.debug(TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()
        LogDog.debug(TAG, "onPause")
    }

    override fun onStop() {
        super.onStop()
        LogDog.debug(TAG, "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        LogDog.debug(TAG, "onDestroy")
    }
}