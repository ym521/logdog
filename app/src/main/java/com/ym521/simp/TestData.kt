package com.ym521.simp

data class TestData(
    val name: String,
    val sex: Boolean,
    val age: Int,
    val height: Int
)