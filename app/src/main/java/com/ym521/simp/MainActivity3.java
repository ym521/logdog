package com.ym521.simp;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ym521.logdog.LogDog;

/**
 * @author Ym
 * E-mail: 2435970206@qq.com
 * createTime:2023/7/23
 * explain:
 */
public class MainActivity3 extends AppCompatActivity {
    private static final String TAG = "MainActivity2";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        LogDog.debug(TAG,"onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        LogDog.debug(TAG,"onStart");
    }


    @Override
    protected void onResume() {
        super.onResume();
        LogDog.debug(TAG,"onResume");
    }
}
