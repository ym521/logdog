package com.ym521.simp

import android.app.Application
import com.google.gson.Gson
import com.ym521.logdog.LogDog
import com.ym521.logdog.core.IJsonEngine

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2025/1/24
 *explain:
 *
 */
class MyApplication2 : Application() {

    override fun onCreate() {
        super.onCreate()
        val gson = Gson()
        LogDog.Builder()
            .install(this, object : IJsonEngine {
                override fun toJSON(obj: Any): String {
                    return gson.toJson(obj)
                }
            })
    }
}