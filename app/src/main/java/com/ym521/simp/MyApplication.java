package com.ym521.simp;

import android.app.Application;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.ym521.logdog.LogDog;
import com.ym521.logdog.core.IJsonEngine;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Gson gson = new Gson();
       new LogDog.Builder().install(this, new IJsonEngine() {
           @NonNull
           @Override
           public String toJSON(@NonNull Object obj) {
               return gson.toJson(obj);
           }
       });
        LogDog.getBuilder()
                .crashHandlerEnable(true)
                .defaultTAG("TST")
                .enabledNewLogCat(true);
    }
}
