package com.ym521.simp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ym521.logdog.LogDog
import com.ym521.logdog.core.LogPriority
import com.ym521.simp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"
    private lateinit var viewBind: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBind = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBind.root)
        LogDog.debug(TAG, "onCreate")
        LogDog.builder?.logShowMethodEnable(true, 3)
//        LogDog.builder?.logWriteFileEnable(true)
//        LogDog.builder?.logWriteFilter(LogPriority.INFO, LogPriority.DEBUG, LogPriority.WARN)
//        LogDog.builder?.logFilter(LogPriority.DEBUG,LogPriority.INFO,LogPriority.ERROR)
        LogDog.debug("CS", "开始测试第一例  debug")
        LogDog.info("CS", "开始测试第一例  info")
        LogDog.warn("CS", "开始测试第一例  warn")
        LogDog.error("CS", "开始测试第一例  error")
        val intAray = mutableListOf(1, 2, 3, 4, 5, 6, 7, 8, 9)
        LogDog.info("SZ", intAray)
        val dataList = mutableListOf(
            TestData("1001", true, 100, 180),
            TestData("1002", false, 20, 170),
            TestData("1003", true, 30, 185)
        )
        LogDog.debugf(
            "1003",
            "%log (Optional) Whether to show thread info or %log not. Default true",
            "11111",
            dataList
        )
        viewBind.tvDedug.setOnClickListener {
            LogDog.warn("CS", "开始测试第一例  warn")
        }
        setLog()

        Thread {
            for (i in 0..5) {
                LogDog.debug(
                    "Thread_1",
                    "(Optional) Whether to show thread info or not. Default true"
                )
                LogDog.error("Thread_1", "(Optional) How many method line to show. Default 2")
                LogDog.info(
                    "Thread_1",
                    "(Optional) Skips some method invokes in stack trace. Default 5"
                )
                LogDog.custom(
                    true,
                    false,
                    LogPriority.WARN,
                    "Thread_1",
                    "Skips some method invokes in stack trace. Default 5555"
                )

                LogDog.customf(
                    true,
                    false,
                    LogPriority.WARN,
                    "Thread_1",
                    "Skips some method invokes in stack trace. Default %log",
                    8888
                )
            }
        }.start()
        Thread {
            for (i in 0..5) {
                LogDog.debug(
                    "Thread_2",
                    "(Optional) Whether to show thread info or not. Default true"
                )
                LogDog.error("Thread_2", "(Optional) How many method line to show. Default 2")
                LogDog.info(
                    "Thread_2",
                    "(Optional) Skips some method invokes in stack trace. Default 5"
                )
            }
        }.start()

//        findViewById<View>(R.id.tvDedug).postDelayed({
//            startActivity(Intent(this, MainActivity2::class.java))
//            finish()
//            view.post {
//                LogDog.info(
//                    "Thread_2",
//                    "(Optional) Skips some method invokes in stack trace. Default 5"
//                )
//            }
//        }, 5 * 1000)
    }


    private fun setLog() {
        initView()
    }

    private fun initConfig() {
        LogDog.debug("(Optional) Whether to show thread info or not. Default true")
        LogDog.error("(Optional) How many method line to show. Default 2")
        LogDog.info("(Optional) Skips some method invokes in stack trace. Default 5")
    }

    private fun initView() {
        LogDog.debug("(Optional) Whether to show thread info or not. Default true")
        LogDog.info("(Optional) How many method line to show. Default 2")
        LogDog.warn("(Optional) Skips some method invokes in stack trace. Default 5")
        initConfig()
    }

    override fun onStart() {
        super.onStart()
        LogDog.debug(TAG, "onStart")
    }

    override fun onResume() {
        super.onResume()
        LogDog.debug(TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()
        LogDog.debug(TAG, "onPause")
    }

    override fun onStop() {
        super.onStop()
        LogDog.debug(TAG, "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        LogDog.debug(TAG, "onDestroy")
    }


}