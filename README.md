# LogDog(日志狗)

#### 介绍

LogDog 是一个开源的 Java 日志框架，它提供了一种简单、高效的方式来管理应用程序的日志。它的特点如下：

1. 简单易用：LogDog 提供了一个简单的 API，可以使用它来记录日志，而不需要写复杂的代码。

2. 多种输出方式：LogDog 支持多种输出方式，包括控制台输出、log文件输出、logcat 输出等。

3. 日志级别控制：LogDog 提供了多种日志级别控制，包括 DEBUG、INFO、WARN、ERROR等。

4. 可扩展性：开发人员可以通过封装进行扩展，开发者可以自定义输出方式和日志格式等。

5. 性能优化：LogDog 使用异步方式进行日志处理，可以提高日志记录的性能。

总的来说，LogDog 是一个功能强大、易用性高、性能优秀的 Android日志框架。

#### LogDog优点

1. ###### 打印的格式

日志格式采用同一时刻打印，如果超出打印限制则分多次打印，同一时刻打印和较少的日志分割符便用开发人员复制日志信息，而不会因为复制日志造成复制了多余的日志分割符从而不利于开发。

```logcatfilter

    ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━start━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
         //当前线程信息
    ┣┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┫
         // 当前方法栈信息
    ┣┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┫
        //需打印的信息
    ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━end━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛


```

2. ###### 日志信息

除了提供基本的打印信息，还提供了打印当前线程信息，当前方法栈信息：
大概就是四行三块；
第一行是日志开始分割符 注明start；
第一块是线程信息 需要开发人员自行设置开启；
第二行、第三行都是日志的中部信息间隔符；
第二块是当前方法栈信息点击方法内的信息就可直接跳转对应的行数，按顺序打印，所以最后一个方法其实就是打印函数调用的方法；
第三块是需要打印的信息；
第四行是一条日志结束的分割符号。

```logcatfilter
    ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━start━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
        currentThread: main
    ┣┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┫
        com.ym521.simp.MainActivity.setLog(MainActivity:23)
            com.ym521.simp.MainActivity.initView(MainActivity:37)
                com.ym521.simp.MainActivity.initConfig(MainActivity:28)
    ┣┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┫
    开始测试第一例
    ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━end━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

```

3. ###### LogDog的使用便利

LogDog使用koltin开发，在对外使用的方法，都采用静态函数（方法）：

```kotlin
  LogDog.debug(tag, msg) //对应 Log.d 

LogDog.info(tag, msg) //对应 Log.i

LogDog.warn(tag, msg) //对应 Log.w

LogDog.error(tag, msg)//对应 Log.e

```

如上，所有打印函数是可以字节类名调用的，前提是LogDog已实现初始化。

4. ###### 参数优化

其他Log框架对打印参数都做了限定，LogDog不在限制开发人员打印的参数进行限制，可以是对象、基本类型、数组等都可以；总是在打印
引用类型时会使用Json引擎进行Json转换，JsonEngine LogDog只提供相同的接口，方便开发人员对其自定义实现，采用何种Json框架由开发人员自己决定；当然
这样也就产生了Json引擎是初始化时必要的参数。

5. ###### 日志文件输出

Android 原生Log未提供日志文件的写入，LogDog提供了一套以日期中的天为单位的日志文件写入引擎；因为是以天为日志文件单位，所有一天之内的日志都只会在一个文件内；为了避免
过多的权限申请，所以日志文件采用APP内部私有空间，但是为了方便日志文件的外部提取，LogDog也提供APP外部私有空间的存储，只需要在初始化LogDog 进行配置开启就可以了，因为都是私有空间所以都
不用权限申请。

6. ###### 日志文件的内容加密

有时日志内容涉及用户和系统、APP的涉密信息，所以需要对日志文件内的内容进行加密，避免在用户提取日志文件时造成信息泄密威胁运行的系统安全，所以日志文件加密很重要，当然LogDog也提供了日志文件加密
只需要在初始化LogDog 进行开启AES和配置Key就可以了，并且加密只加密 打印数据部分对于TAG、时间戳 log等级不进行加密，由于加密是采用逐条Log加密的所以解密也需要逐条Log解密；
这个问题后期版本会采用整体加密或者其他加密形式优化这个问题。

#### 安装教程

1. ###### 添加远程仓库

```gradle

maven { url 'https://jitpack.io' }

```

2. ###### 添加依赖

最新版本
[![](https://jitpack.io/v/com.gitee.ym521/logdog.svg)](https://jitpack.io/#com.gitee.ym521/logdog)

```gradle

dependencies {
	        implementation 'com.gitee.ym521:logdog:LatestVersion'
	}

```

#### 使用说明

个人建议最好在自定义的Application的onCreate()中先调用；记得在AndroidManifest.xml 设置自定义的Application！！！

```kotlin

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        //这里使用的是 Google开源 Gson 也可以使用其他Json 框架 如:阿里的FastJson
        val gson = Gson()

        //开发人员自行实现对象转json字符串
        LogDog.Builder()
            .logWriteLogFileEnable(true) //是否开启写入日志文件
            .logShowMethodEnable(true) //是否开启显示方法栈信息 默认打印两个方法信息
            .logShowThreadInfoEnable(true) //是否打印当前线程信息
            .install(this, object : IJsonEngine {
                override fun toJSON(obj: Any): String {
                    return gson.toJson(obj)
                }
            })
    }
}


```

```java

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        //这里使用的是 Google开源 Gson 也可以使用其他Json 框架 如:阿里的FastJson
        Gson gson = new Gson();
        new LogDog.Builder()
                .logWriteLogFileEnable(true) //是否开启写入日志文件
                .logShowMethodEnable(true) //是否开启显示方法栈信息 默认打印两个方法信息
                .logShowThreadInfoEnable(true) //是否打印当前线程信息
                .install(this, new IJsonEngine() {
                    @NonNull
                    @Override
                    public String toJSON(Object obj) {
                        return gson.toJson(obj);
                    }
                });
    }
}


```

#### LogDog功能使用

1. ###### LogDog 配置说明

```kotlin
   LogDog.builder
    .logFilter() //日志过滤 默认全部打印;例如过滤debug、info： logFilter(LogPriority.DEBUG, LogPriority.INFO) 
    .extraOffset(0) //自定义方法/函数栈的打印零点的额外补充调整 范围[-3 ,5]   
    .logShowMethodEnable(false) //是否打印方法信息和打印多少个方法信息 默认2个
    .logShowThreadInfoEnable(false) //是否打印线程信息   默认值 false
    .logWriteFileEnable(false) //是否将日志写入日志文件中 默认值 false
    .logFileAESEnable(
        false,
        "0000000000000000"
    ) //是否启动AES 加密 默认值 false ,可以不配秘钥 默认秘钥： 0000000000000000 注意秘钥字符串转byte数组必须是16个否则开启加密失败
    .logFileExtEnable(false) //是否将日志文件输出到外部私有空间 用于日志文件的提取 默认值 false
    .setDefaultTAG("LOGDOG") //自定义 设置，没有自行填写的TAG的统一缺省TAG 默认值：LOGDOG
    .encodingConfusion(false) //编码混淆 默认不开启 编码混淆只针对 log文件编码混淆 
    // 如果开启 不能在运行中和同一天（同一个日志文件）内发生更改编码混淆开关， 不然会造成 log文件 无法正确解密
    .logFileSize(10) //log文件大小分割 单位：MB
    .enabledNewLogCat(true) //对新版本LogCat的优化

```

V2.3.0 新增log 文件分享，具体新增配置 在AndroidManifest.xml 新增如下：

```xml

<provider android:name="com.ym521.logdog.provider.LogFileShareProvider"
    android:authorities="${applicationId}.fileshare" android:exported="false"
    android:grantUriPermissions="true">
    <meta-data android:name="android.support.FILE_PROVIDER_PATHS"
        android:resource="@xml/provider_paths" />
</provider>
```

使用：调用LogDog.logFileShare() 即可

java 配置信息同上kotlin；
但是需要注意的是：
1、秘钥转byte数组必须是16个否则开启加密不启动，建议可以采用16个非汉字组成秘钥！！！
2、logFileAESEnabl、logFileShowEnable 的启用前提是logWriteLogFileEnable 必须要开启

V2.5.6 优化对新版本LogCat的Log 打印：

                 CS 
                 ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━start━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
                     android.app.Activity.performCreate(Activity:7079) 
                         android.app.Activity.performCreate(Activity:7088) 
                             com.ym521.simp.MainActivity.onCreate(MainActivity:26) 
                 ┣┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┫
                 开始测试第一例  error 
                 ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━end━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

在LogCat新版本中选择 compact view下 将无法显示的tag 添加到 log 顶部  

1. ###### LogDog 功能使用

```kotlin
  LogDog.debug(tag, msg) //对应 Log.d 
LogDog.debugf(tag, msgformat, vararg) //msgformat可以使用String 占位符 %s 且只支持这个占位符

LogDog.info(tag, msg) //对应 Log.i
LogDog.infof(tag, msgformat, vararg) //msgformat可以使用String 占位符 %s 且只支持这个占位符

LogDog.warn(tag, msg) //对应 Log.w
LogDog.warnf(tag, msgformat, vararg) //msgformat可以使用String 占位符 %s 且只支持这个占位符

LogDog.error(tag, msg)//对应 Log.e
LogDog.errorf(tag, msgformat, vararg) //msgformat可以使用String 占位符 %s 且只支持这个占位符

```

```java
//TODO 
// 注意：TAG属于可选参数，可以不填写，msg 不需要区分类型。
// Java使用 参考 Koltin使用示例代码
// msgformat可以使用String 占位符 %log 且只支持这个占位符

```

